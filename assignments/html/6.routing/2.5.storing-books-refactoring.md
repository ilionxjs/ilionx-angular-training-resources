# Storing books
Previously, we used an `@Output` property to notify the parent that a new book needed to be saved.
But since we've put `NewBookComponent` on its own, there is no direct parent anymore to notify. To solve this, we'll create a new specialized BusinessComponent.
This component will have the `BooksService` injected, and will serve as a parent for the `NewBookComponent`. This structure keeps the `NewBookComponent` a reusable Presentation Component.

# SaveBookComponent
1. Create a new component, manually or by generating it with the CLI.
2. Inject the `BooksService`, and move the `save` method from `BooksComponent` to the new component.
4. In the template, add the selector of `NewBookComponent`, and connect its `save` event to the save method on the class.
5. Change the route so that the path `books/new` points to the new component.