# Optional

## More modules
You can use the Angular CLI to easily create more modules and components. We can practice this by creating a few modules and some components. 
Try to setup the modules with Business and Presentation components, and when you need to share functionality over modules put the components in the correct module.

The goal of this assignment is practicing with the Angular CLI, so no templates are made available. 
However, when you are done with the other assignments you could implement one of these features for real.

Below are a few ideas that you can use to generate the necessary modules & components. 

1. ShoppingCart

The shopping cart will hold the items the user has selected, and needs an option to fill in user & buying details.

Here are a few examples of components you could add.
* a page where the content of the shopping cart is shown
* an order page for filling in user details & buying information
* a confirmation page

2. CoffeeShop

What is a book store without a coffee? This bookstore also has a coffee order page, with coffees, cups, special flavours of sugar and so on.
Just like the Books, the user needs to be able to buy these and put them in the Shopping Cart.
This means that the ShoppingCart should be able to show multiple types of items.

3. BackOffice

The managers of the Book store might want an overview of all the items that are currently available in the store, in a nice overview.
It would also be nice to get a report of all the sales, and of course how much income has been generated.

4. Request an item

Not all books or coffees are available all the time. It would be nice if the user has some functionality where they can request books or coffees and get a notification 
when the item has arrived.
