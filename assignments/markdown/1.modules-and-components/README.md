# Modules and Components

An Angular-application consists of Modules with each their own tree of components.
Remember the difference between certain 'types' of modules and the different style of components.

In this assignment we'll setup the Angular application from the start, and convert the current static page to an Angular application.
We'll do this in 3 steps:
* Create a Root component & Root module
* Create a specialized component for the books
* Refactor the code to use a Feature Module


## Techniques
* [@Component](https://angular.io/guide/architecture#components)
* [@NgModule](https://angular.io/guide/architecture#modules)
* [Frequently used NgModules](https://angular.io/guide/frequent-ngmodules)
* [Types of NgModules](https://angular.io/guide/module-types)

## Tree diagram
![Components](../../images/1.png)
