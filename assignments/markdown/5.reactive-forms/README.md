# Reactive Forms

Almost every application has a form in which users can enter data. Such a form usually has validation for the inputs, 
and tells the user when a field has a wrong value. The form is then sent to the backend for further validation of 
business rules, and eventually saved.

In this assignment, we will make a form that will store a new book on the server.

## [Techniques](https://angular.io/guide/reactive-forms)
* FormGroup, FormControl
* `(ngSubmit)`
* [formGroup], formControlName
* [validation](https://angular.io/guide/form-validation#reactive-form-validation)

## Tree diagram
![Components](../../images/5.png)
