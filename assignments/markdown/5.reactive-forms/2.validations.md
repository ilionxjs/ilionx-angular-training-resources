## Validations
The fields `author` and `title` need to be required, and luckily most books have those properties.

1. To make the fields required, the `FormControl` objects need a [Validator](https://angular.io/api/forms/Validators). You can pass in an array of `Validator`s as a second parameter of `FormControl`.
    the first parameter is the initial value for the form, which can be `''` for an empty field.

2. Show a message(see below) for every field which is not valid. Put the messages just below `<!-- error message -->`
  ```html
<span class="g--8 m--4 color--alizarin">This field is required</span>
```

3. Make the error messages in the template visible when a field is invalid and touched. Use a `*structuralDirective` to optionally show the element.
    To get the state of any field, you can use the following syntax in the template.
    ```javascript
       bookForm.get('title').hasError('required')
       bookForm.get('title').invalid
       bookForm.get('title').dirty
       bookForm.get('title').touched
       bookForm.get('title').pristine
    ```

4. As you might have noticed, there is a lot of duplication by getting the field from the form every time. This can be made more DRY by defining a `getter` on `NewBookComponent`.

  **new-book.component.ts**
  ```javascript
    get title() {
      return this.bookForm.get('title');
    }
  ```
   **new-book.component.html**
  ```html
    <span *ngIf="title.invalid"></span>
  ```
