## Optional

## Error handling
The book server checks if the book has already been stored, by checking the title & author. If the book is already in the shop,
it will return an error with code `422` and a message.

1. Show the message from the server in the UI when a book is added with the same title and author.
   Try to keep the `HttpErrorResponse` class scoped in the service, this means that the component won't have knowledge of this class.
   To rethrow the error from the service so it will end up in the error handler of the component, you need the `catchError` & `throwError` operators.
   ```javascript
   import {catchError} from 'rxjs/operators';
   import {throwError} from 'rxjs';
   
   http.post()
     .pipe(
       catchError(error: HttpErrorResponse) => {
         // do some checking on the type of error.
         const customError = ...
         return throwError(customError);
       }
     )
   ```


## custom form control
You can make any component a `FormControl`, and add it to the form object. These `FormControl`s work in both Reactive forms and Template forms.
See [Custom Form Control](https://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html) as a tutorial.

1. Add an custom form control for `price`, or create your own.

```javascript
<div class="g--12">
  <div class="container">

    <label for="price" class="g--4 no-margin"> Price </label>
    <input type="text" class="g--8 no-margin" id="price">
  </div>
  <!-- error message -->
</div>
```

2. The price has to conform to the format ##(.##), and no book is more expensive then 999.99.
    Validate the input for these cases and show an error message for the user.
    You can use the following regular expression if necessary.
   ```^\d{1,3}([\.]\d{2})?$```

## ngClass
Give the input fields a visual indication when they are invalid. The class `field-invalid` can be put on `<input>` to achieve this.

```javascript
[ngClass]="{'className': boolVal}"
or
[class.CLASSNAME]="boolVal"
```

## delete
Now that we can add books, it would also be nice to be able to remove a book.
There is an endpoint available on `/book/:id` which handles http `DELETE` calls to delete a book from the server. The placeholder `:id` is meant for the ID of the book.
This endpoint gives you an empty response with status code 204 when the deletion was successful.

When the book, with id 100004 for example, is not found, you will get the a 404 response with the following body
```javascript
{ detail: `Book: 100004 could not be found`, deleted: false }
```

1. Create a method in `BooksService` that will execute the delete.

2. Add a button to the application which will eventually call the delete method in `BooksService`. Remember the responsibilities of the different components, and put the code in the right places.
```javascript
<button class="g--12 btn--red color--white">Delete</button>
```

3. For now, you have to refresh to see the deleted book be removed from the screen. In a following optional assignment we will try to update the view automatically.

4. Show a message on screen if something went wrong with deleting the book. Try to delete the same book twice, to get the error message from the backend.

## FormBuilder
Angular provides a [FormBuilder](https://angular.io/guide/reactive-forms#using-the-formbuilder-service-to-generate-controls) service to remove some of the boilerplate code required with `FormGroup`/`FormControl` classes.

1. Rebuild the form in `NewBookComponent` to use the `FormBuilder` service.

## Nested FormGroup
It's also possible to [nest FormGroups](https://angular.io/guide/reactive-forms#creating-nested-form-groups) inside other `FormGroup`s. In our book example, you might want to store some meta information about a book, like the date it was added to the store or the ISBN identifier.
With a nested `FormGroup`, you can combine the date and ISBN under a group.
Currently, when you get the `.value` of the `bookForm`, you'll get a response like:
```javascript
{
  "title": "Application Design",
  "author": "O'rly",
  ...
}
```
With nested `FormGroup`s, the `.value` of `bookForm` could look like this:
```javascript
{
  "title": "Application Design",
  "author": "O'rly",
  "meta": {
    "date": "1970-01-01",
    "ISBN": "1234"
  }
}
```

1. Add two new fields to the form in a [nested `FormGroup`](https://angular.io/guide/reactive-forms#creating-nested-form-groups).

## FormArray
The [FormArray](https://angular.io/guide/reactive-forms#creating-dynamic-forms) class makes is possible to create dynamic forms, where you can add or remove fields or `FormGroups` from the form.
This is useful when you want to have a domain object with a property that can have multiple values. In our book-example, this could be the `genre`-field.
There are many books that do not fit into one genre, but need te be categorized multiple.

1. Use [FormArray](https://angular.io/guide/reactive-forms#creating-dynamic-forms) to dynamically add multiple genres to a new book.

## listeners
With `.valueChanges` and `.statusChanges`, which are properties of `FormGroup` and `FormControl`, you can listen for any new value or when the validation status of the field changes.
By reacting to input changes it's easier te make something like an autocomplete field.

1. Listen for changes on the `title`-field using `.valueChanges`, and make a GET request on `http://localhost:3004/book/exists/:title` to see if the title already exists.
  The endpoint responds with an empty list when there are no matches, or a filled list with all book where the start of the title matches exactly as entered.

## Edit existing books
The `NewBookComponent` could be reused to also support editing of existing books. 
What changes would you need to make to get `NewBookComponent` reusable for existing data?

1. Add support to edit existing books to `NewBookComponent`

## custom validator
The `Validator`s that are added to the `FormControl`s are just functions that return `null` when valid, or an error object when the field is invalid.

1. Write a [custom validator](https://angular.io/guide/form-validation#custom-validators) for `author`, that checks if each word in the input is capitalized. Show an error to the user, telling them which word needs to be capitalized.
