## Route Parameter Observable.
It's also possible to define routes with parameters. For practice we'll make a page that will show the details of 1 book.
There is an endpoint available on `localhost:3004/book/:id` where you can get a book by their ID.

1. In `BooksService`, create a method to get a book by id. Use `getBooks` as an example on how to do a GET call.

2. If you have not yet made a separate component for a single book `book.component.ts`, do this now.

In order to reuse the Presentation Component `BookComponent`, we need a new Business Component that will read the URL for the book id, 
fetches the book from the `BookService` and pass it into `BookComponent`.

3. Create a new Component that will read the URL and fetches the book via the service. Try to think of a good name.

4. Inject `ActivatedRoute` and `BooksService` in the constructor of your new Component.

5. See presentation `5.Routing`, slide 18 to see the possible ways to interact with `ActivatedRoute`. Which one do you choose?

8. Use the id from the URL to call the method in `BooksService`.

9. Use the `async`-pipe to pass the book into `BookComponent`.
