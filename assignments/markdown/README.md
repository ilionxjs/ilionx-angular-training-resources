# Start

Before we can start building Angular applications we have to prepare the development environment.
This means installing the dependencies with NPM and start the application with the CLI to have a running application.

## Tasks

1. Import the project into your IDE.
    
2. Run the following command from the command line, on the same level as `package.json`. Or use the IDE.
    ```javascript
    npm install
    ```
    
    This will download the Angular framework and all of the dependencies.
  
3. Start the application server and TypeScript-transpiler in a terminal or from your IDE:
    ```javascript
    npm start
    ```

## Optional: Angular DevTools

The [Angular devtools](https://angular.io/guide/devtools) plugin is a powerful debugging tool for Angular.
If you use Chrome, this extension can give you an in depth view of the application.
   
# FAQ
```javascript
npm install
```
If the install has not run properly due to network issues or environment setting, it is possible to copy them from someone else.
