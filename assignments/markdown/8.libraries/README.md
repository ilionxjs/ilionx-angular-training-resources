# Libraries
There are a lot of useful libraries available on NPM, which will speed up the development of your application.
A few of the most popular types of library add UI components like tables or datepickers, or highly optimized functions like lodash.

In this assignment we will add a table to the application from the [Angular Material](https://material.angular.io) library.

We will make a new module called 'backoffice', that will show the book inventory in a table and is available on its own url.

## Techniques
* adding a library to the application
* importing functionality from a library
* using library documentation & examples

## [Angular Material Component Developer Kit (CDK)](https://material.angular.io/cdk)
The CDK provides un-opinionated components to be used by the application. 
The main goal of the CDK is add very general functionality, which can easily be customized by the developers.

* [CDK table](https://material.angular.io/guide/cdk-table)
* [CDK table api](https://material.angular.io/cdk/table/api)
