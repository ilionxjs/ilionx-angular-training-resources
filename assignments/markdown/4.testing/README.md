# Testing

Testing ensures your application will continue working as designed when altering or refactoring code. 
They can also ensure the correct functioning of Angular components. Test files are near the code in `.spec.ts` files.

## [Techniques](https://angular.io/guide/testing) 
* [Jasmine](https://jasmine.github.io/api/4.1/global)
* TestBed
* `configureTestingModule`
* `compileComponents`
* `TestBed.inject()`

> **Red bar**
>
> Make the test fail by expecting a length other than 2.
> Also check what will happen when the test has NOT been wrapped with `async()`

