## book-reserved.directive test
To test directives, we need a 'HostComponent' for the test that will have the directive added to the template.
We need this host component to get a reference to the HTMLElement that the directive needs to style.

1. If not already generated, create a `book-reserved.directive.spec.ts` file next to the `book-reserved.directive.ts`

*HostComponent*
We need to create a HostComponent that is used for the test. We can do this in the same file as the test itself.
```javascript
@Component({
})
class TestHostBookReservedComponent {

}
```

1. Set the `template` property of the component. This is the only required property of a component.
  Add a `<button>` and add the `BookReserved` directive with the required inputs.
  
2. Add a property called `reserved` to the `TestHostBookReservedComponent` class.
    We will use this property to toggle the border for the element the directive is on.

3. Import the `BookReservedDirective`.

4. Import `TestBed` from `'@angular/core/testing'`;

5. Start with `beforeEach(() => TestBed.configureTestingModule({}))` and add the directive *and* host component to the
    `declarations: []`.

5. In the same `beforeEach` method call `compileComponents()` of `TestBed`.

6. Create 2 new tests in the `describe()` using `it()`. Try to fit the following sentence into the `describe` and `it`s:
  'When the passed in value is true, the element gets a border' and  'When the passed in value is false, the element does not have a border'

### ComponentFixture
> **ComponentFixture properties**
>
> * `componentInstance`: allows interaction with the *class*.
> * `debugElement`: allows interaction with the *template*.

1. Call `TestBed.createComponent(TestHostBookReservedComponent)` in the test. This returns a `ComponentFixture`.
2. Assign this value to a variable `fixture: ComponentFixture<TestHostBookReservedComponent>`.

3. Set the `reserved` property of `fixture.componentInstance` to `true` for the first test.

4. Call `fixture.detectChanges();` to start the change detection process from Angular which will update the template based on the values.

7. Add an expect to the number of elements in the list. We've mocked the input with a list of 2 books.
    ```javascript
    let button = fixture.debugElement.query(By.css('button'));
    expect(button.styles.border).toBe('2x solid #0000FF');
    ```
8. Add a test for the directive when the `reserved` property is false. The border should be an empty string.
    
9. Run the tests by executing `ng test` on the command line.
