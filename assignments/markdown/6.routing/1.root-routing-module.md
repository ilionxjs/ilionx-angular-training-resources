## app-routing.module.ts

1. Create a new file for the Root routing called `app-routing.module.ts`.

2. Create a variable called `APP_ROUTES` of type Route[]. Assign it an empty array.

3. Create a new `Route` in the array with an empty `path` `''` and `component` to `BooksComponent`
   
4. Create a new NgModule, called `AppRoutingModule`, put `RouterModule.forRoot(APP_ROUTES)` into the `imports` array.

    > **RouterModule.forRoot()**
    >
    > With forRoot() an instance of the Router service is created. It is *important* that forRoot is used once in the entire application.
    > The Root Module should be the only place where forRoot() is used.

5. Add the property `exports` to the NgModule and add `RouterModule` to it.

   > Exporting RouterModule makes the `<router-outlet>`-tag available in other modules importing the AppRoutingModule.

6. Import `AppRoutingModule` in `AppModule` and add this to the `imports` property of `NgModule`.

7. In the template of `AppComponent`, replace the tag `<ibs-books></ibs-books>` with `<router-outlet></router-outlet>`.
