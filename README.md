# Installation
`npm install`

# Start assignments in browser
`npm start`

* `assignments/markdown/`
Contains the assignments in markdown format.

* `assignments/html/`
Is used to run the assignments in the browser
